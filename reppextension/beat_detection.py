####################################################################################################
# File:     beat_detection.py
# Purpose:  Main functions for beat detection task,
#           it assumes stimulus consists of a defined "token" that repeats e.g. x_xx|x_xx|x_xx,
#           it does NOT assume that subject taps to every element of the token
#
# Author:   Vani Rajendran
#
####################################################################################################
from __future__ import division
import numpy as np
import gc
from matplotlib import pyplot as plt
# import numpy.matlib

import repp.analysis
from repp import signal_processing as sp
from repp.analysis import REPPAnalysis
from scipy.stats import circmean, circstd
from scipy import signal
import scipy.io.wavfile as wav


# Analysis
class REPPAnalysisBeatDetection(REPPAnalysis):
    """ A subclass of REPPAnalysis for beat detection tasks

    Attributes
    ----------

    config : class
        Configuration parameters for the experiment
    """

    def __init__(self, config):
        super().__init__(config)
        self.config = config

    def do_beat_detection_analysis(
            self,
            recording_filename,
            stim_prepared_filename,
            stim_info,
            title_in_graph,
            output_plot
    ):
        """ Extract tapping and marker onsets from
        music beat detection task, but taking stim_onsets directly (VR 12 oct 2021)
        """
        print("Extracting audio signals from mono recording...")
        audio_signals = sp.extract_audio_signals(recording_filename, self.config)
        initial_onsets = sp.prepare_onsets_extraction(
            stim_info['markers_onsets'],
            stim_info['stim_shifted_onsets'],
            stim_info['onset_is_played'])
        print("Extracting raw onsets from  audio signals...")
        raw_extracted_onsets = sp.extract_onsets(audio_signals, self.config)
        print("Aligning tapping onsets with marker onsets...")
        aligned_onsets = align_onsets_beat_detection(initial_onsets,
                                                     raw_extracted_onsets,
                                                     self.config.MARKERS_MATCHING_WINDOW,
                                                     self.config.ONSET_MATCHING_WINDOW_PHASE
                                                     )

        output, analysis, is_failed = do_stats(aligned_onsets, self.config)
        fig = self.do_plot_beat_detection(
            title_in_graph,
            audio_signals,
            aligned_onsets,
            analysis,
            is_failed,
            self.config,
            stim_info,
            stim_prepared_filename
        )
        self.save_local(fig, output_plot, dpi=300)  # save local
        print("Plot saved")
        del fig
        gc.collect()

        return output, analysis, is_failed  # audio_signals, raw_extracted_onsets, aligned_onsets,

    def compute_feedback_practice(self, recording_filename, stim_info, output_plot):
        """ Extract tapping and marker onsets from
        music beat detection task, but taking stim_onsets directly (VR 12 oct 2021)
        """

        print("Preparing marker onsets...")
        markers_onsets = np.array(stim_info['markers_onsets'])
        print("Extracting audio signals from mono recording...")
        audio_signals = sp.extract_audio_signals(recording_filename, self.config)
        print("Extracting raw onsets from  audio signals...")
        raw_extracted_onsets = sp.extract_onsets(audio_signals, self.config)
        print("Aligning tapping onsets with marker onsets...")
        aligned_onsets, some_stats = align_tapping_with_markers(
            markers_onsets,
            raw_extracted_onsets,
            self.config.MARKERS_MATCHING_WINDOW,
            self.config.ONSET_MATCHING_WINDOW_PHASE,
            self.config,  # config and stim_info necessary to check whether taps are isochronous
            stim_info
        )
        print("Aligning stimulus onsets with stim beginning...")
        stim_onsets = stim_info['stim_onsets']

        fig = plot_async_feedback(aligned_onsets, self.config)
        self.save_local(fig, output_plot, dpi=300)  # save local
        print("Plot saved")
        del fig
        gc.collect()
        return audio_signals, raw_extracted_onsets, aligned_onsets, stim_onsets, some_stats

    def extract_markers_only(self, recording_filename, stim_info):
        """ Extract tapping and marker onsets from
        music beat detection task
        """
        print("Preparing marker onsets...")
        markers_onsets = np.array(stim_info['markers_onsets'])
        print("Extracting audio signals from mono recording...")
        audio_signals = sp.extract_audio_signals(recording_filename, self.config)
        print("Extracting raw onsets from  audio signals...")
        raw_extracted_onsets = sp.extract_onsets(audio_signals, self.config)
        print("Aligning tapping onsets with marker onsets...")
        aligned_onsets, some_stats = align_tapping_with_markers(
            markers_onsets,
            raw_extracted_onsets,
            self.config.MARKERS_MATCHING_WINDOW,
            self.config.ONSET_MATCHING_WINDOW_MS,
            self.config.ONSET_MATCHING_WINDOW_PHASE)
        print("Aligning stimulus onsets with stim beginning...")
        stim_onsets = stim_info['stim_onsets']

        return audio_signals, raw_extracted_onsets, aligned_onsets, stim_onsets

    def do_plot_beat_detection(
            self,
            title_plot,
            audio_signals,
            aligned_onsets,
            analysis,
            is_failed,
            config,
            stim_info,
            stim_prepared_filename
    ):
        """ Plot results

        Parameters
        ----------

        title_plot : str
            title of the plot
        audio_signals : dict
            A dictionary with the output of the signal extraction and cleaning procedure
        aligned_onsets : dict
            A dictionary with the output of the onset alignemnet procedure
        analysis : dict
            A dictionary with the output of the performance analysis
        is_failed : dict
            A dictionary with the output failing criteria
        config : class
            Configuration parameters for the experiment (see ``config.py``)
        stim_info : dict
            A dictionary with stimulus info
        stim_prepared_filename : str
            Stimulus filename

        Returns
        -------

        fig : class
            Figure with main results
        """
        tt = audio_signals['time_line_for_sample']
        mxlim = [min(tt), max(tt)]
        plt.clf()
        fig = plt.figure()

        # first raw:
        repp.analysis.plot_original_recording(title_plot, mxlim, tt, audio_signals,
                                              aligned_onsets, is_failed, 1, config)
        plot_tapping_rec(tt, mxlim, audio_signals, aligned_onsets, analysis, 2, config)
        plot_tapping_zoomed(tt, audio_signals, aligned_onsets, analysis, 3, config)

        # second raw:
        repp.analysis.plot_markers_detection(mxlim, tt, audio_signals, aligned_onsets, analysis, 5, config)
        repp.analysis.plot_markers_cleaned(mxlim, tt, audio_signals, aligned_onsets, 6, config)
        repp.analysis.plot_markers_zoomed(tt, audio_signals, aligned_onsets, 7, config)
        plot_markers_zoomed_end(tt, audio_signals, aligned_onsets, 8, config)
        repp.analysis.plot_markers_error(mxlim, aligned_onsets, analysis, 4, config)
        # third raw:
        if int(analysis["num_resp_raw_all"] > 1):
            plot_hist_iti(mxlim, aligned_onsets, analysis, 10, config)  # VR: plot histogram of inter-tap intervals
            plot_cycle_rasters(fig, analysis, 12, config, stim_info, stim_prepared_filename)

        # other plots not used
        fig = plt.gcf()
        fig.set_size_inches(25, 10.5)
        return fig


############################################
# supporting functions for signal processing
############################################


def align_onsets_beat_detection(
        initial_onsets,
        raw_extracted_onsets,
        markers_matching_window,
        onset_matching_window_phase
):
    """
    Align tapping and stimulus onsets, as well as detected and known markers onsets
    NOTE VR 22 oct 2021: copied from repp.signal_processing and modified to NOT try to align taps with stimulus onsets

    :type initial_onsets: dict
    :param initial_onsets: list of initial onsets including stimulus and markers

    :type raw_extracted_onsets: dict
    :param raw_extracted_onsets: list of extracted onsets including tapping and markers

    :type markers_matching_window: float
    :param markers_matching_window: Matching window to detect markers onsets corresponding to the known markers onsets.

    :type markers_matching_window: float
    :param markers_matching_window: Matching window to detect tapping onsets corresponding to the stimulus onsets.

    :type onset_matching_window_phase: float
    :param onset_matching_window_phase: Matching window to detect tapping onsets corresponding to the stimulus onsets.

    :return: Returns re-aligned onsets with key information (dict).
    """
    # get extracted onsets
    markers_detected_onsets = raw_extracted_onsets['markers_detected_onsets']
    tapping_detected_onsets = raw_extracted_onsets['tapping_detected_onsets']

    # crashes if markers_detected_onsets is empty, so check and populate if empty (trial will still fail)
    if len(markers_detected_onsets) == 0:
        markers_detected_onsets = np.array([0])

    # use marker onsets to define a window for cleaning tapping onsets near markers
    markers_onsets = initial_onsets['markers_onsets']
    min_marker_isi = np.min(np.diff(np.array(markers_onsets)))

    # get initial onsets:
    stim_onsets = initial_onsets['stim_onsets']  # actually stim_shifted_onsets by adding config.STIM_BEGINNING
    stim_onsets_played = initial_onsets['stim_onsets_played']
    # correct onsets start point using the first markers as the single frame of reference
    stim_onsets_corrected = stim_onsets - markers_onsets[0]
    stim_onsets_played_corrected = stim_onsets_played - markers_onsets[0]
    stim_onsets_played_corrected = np.less(
        [min(np.abs(onset - stim_onsets_played_corrected)) for onset in stim_onsets_corrected], 1.0)
    tapping_onsets_corrected = tapping_detected_onsets - markers_detected_onsets[0]
    # ideal  version of markers
    markers_onsets_aligned = markers_onsets - markers_onsets[0] + markers_detected_onsets[0]
    # find artifacts and remove
    markers_in_tapping = np.less([(min(np.abs(onset - markers_onsets_aligned))) for onset in tapping_detected_onsets],
                                 min_marker_isi)  # find markers in the tapping signal: Note changed from
    # onset_matching_window to something based on marker ISI
    onsets_before_after_markers = np.logical_or(np.less(tapping_detected_onsets, min(markers_onsets_aligned)),
                                                np.less(max(markers_onsets_aligned),
                                                        tapping_detected_onsets))  # find onsets before/ after markers
    is_tapping_ok = np.logical_and(~markers_in_tapping, ~onsets_before_after_markers)  # are the tapping onsets ok?
    tapping_onsets_corrected = tapping_onsets_corrected[
        is_tapping_ok]  # filter markers from tapping (using corrected version)
    tapping_detected_onsets = tapping_detected_onsets[is_tapping_ok]  # filter markers from tapping (using raw version)
    stim_detected_onsets = stim_onsets - markers_onsets[0] + markers_detected_onsets[0]

    # verify markers
    onsets_detection_info = sp.verify_onsets_detection(markers_detected_onsets, markers_onsets, markers_matching_window,
                                                       onset_matching_window_phase)
    # tapping
    delayms_tofirstdetectedmarker = markers_detected_onsets[0] - markers_onsets[0]

    print("response-stimulus asynchronies = ")
    aligned_onsets = {
        'stim_onsets_input': stim_onsets,
        'stim_onsets_detected': stim_detected_onsets,
        'resp_onsets_detected': tapping_detected_onsets,
        'stim_onsets_is_played': stim_onsets_played_corrected,
        'stim_onsets_aligned': stim_detected_onsets,
        'resp_onsets_aligned': tapping_onsets_corrected,
        'num_resp_raw_onsets': float(np.size(tapping_detected_onsets)),
        'num_stim_raw_onsets': float(np.size(stim_onsets, 0)),
        'markers_onsets_detected': markers_detected_onsets,
        'markers_onsets_aligned': markers_onsets_aligned,
        'markers_onsets_input': markers_onsets,
        'delayms_tofirstdetectedmarker': delayms_tofirstdetectedmarker,
        **onsets_detection_info  # info about markers detection procedure
    }
    return aligned_onsets


###################################
# supporting functions for analysis
###################################

def do_stats(onsets_aligned, config):  # copied + modified from REPPAnalysis
    """ calculate main stats for sms experiments

        Parameters
        ----------

        onsets_aligned : dict
            A dictionary with the output of the onset alignment procedure
        config : class
            Configuration parameters for the experiment (see ``config.py``)

        Returns
        -------

        output : dict
            A dictionary with the output of the signal processing step
        analysis : dict
            A dictionary with the output of the performance analysis
        is_failed : dict
            A dictionary with the output of the failing criteria
        """
    output = reformat_output(onsets_aligned)  # return in good format trial output

    # check if markers are good to go
    if (abs(onsets_aligned['verify_max_difference']) < config.MARKERS_MAX_ERROR) and (
            onsets_aligned['verify_num_missed'] == 0):
        markers_status = "Good"
        markers_ok = True
    else:
        markers_status = "Bad"
        markers_ok = False

    # check if tapping is isochronous!
    R = onsets_aligned['resp_onsets_detected']
    S = onsets_aligned['stim_onsets_detected']

    if len(R) > 1:
        firstid = config.ID_OF_CYCLE_START  # npercycle * ncyc_baseline
        cyclestart_ms = S[firstid]
        npercycle = config.N_ONSETS_PER_CYCLE
        taps_cycle = R - cyclestart_ms  # zero is onset of first cycle of rhythm
        onsets_cycle = S - cyclestart_ms  # stim also starts at 0

        cycle_points = onsets_cycle[firstid:firstid + npercycle + 1] - onsets_cycle[firstid]
        cycle_ints = np.diff(cycle_points)
        cycledur = np.sum(cycle_ints)
        intervals = np.diff(cycle_points)
        which_cyc = taps_cycle // cycledur  # which cycle number
        taps_rel = np.mod(taps_cycle, cycledur)  # remainder is time in cycle

        iti = np.diff(R)
        iti_med = np.median(iti)

        if len(iti) < 5:
            iti_std = np.std(iti)
        else:
            iti_sort = np.sort(iti)
            iti_clean = iti_sort[:-4]  # thorw out 4 largest values
            iti_std = np.std(iti_clean)

        possibilities = [cycledur / 8, cycledur / 6, cycledur / 4, cycledur / 3, cycledur / 2, cycledur, cycledur * 2]
        errors = [abs(iti_med - p) for p in possibilities]
        whichp = [e < 20 for e in errors]

        if True in whichp and iti_std / iti_med < 0.3:
            index = whichp.index(True)
            tapcycledur = possibilities[index]
            STEADYTAPPING = True
        else:
            STEADYTAPPING = False
            tapcycledur = cycledur

        taps_rel_tapcycle = np.mod(taps_cycle, tapcycledur)
        which_cyc_tapcycle = taps_rel_tapcycle // tapcycledur
        tap_rad = taps_rel_tapcycle * 2 * np.pi / tapcycledur
        tap_rad_cycledur = taps_rel * 2 * np.pi / cycledur
        circ_mean = circmean(tap_rad)
        circ_std = circstd(tap_rad)

        # compute async_ms and async_std_ms based on circular mean+std
        async_ms = circ_mean * tapcycledur / 2 / np.pi
        async_std_ms = circ_std * tapcycledur / 2 / np.pi

        # is async negative?
        if abs(async_ms - tapcycledur) < async_ms:
            async_ms = async_ms - tapcycledur

        # for every tap, just find distance from nearest onset
        async_naive = np.array([])
        for r in taps_cycle:
            tmp = r - onsets_cycle
            abstmp = abs(tmp)
            val, idx = min((val, idx) for (idx, val) in enumerate(abstmp))
            thisasync = tmp[idx]
            async_naive = np.append(async_naive, thisasync)
        async_naive_frac = async_naive / tapcycledur
    else:
        iti = []
        iti_med = []
        iti_std = []
        STEADYTAPPING = False
        taps_rel = []
        which_cyc = []
        taps_rel_tapcycle = []
        which_cyc_tapcycle = []
        tap_rad = []
        tap_rad_cycledur = []
        circ_mean = []
        circ_std = []
        async_ms = []
        async_std_ms = []
        cycledur = []
        tapcycledur = []
        cycle_points = []
        cycle_ints = []
        cyclestart_ms = []
        intervals = []
        async_naive = []
        async_naive_frac = []

    analysis = {  # NOW CONTAINS MARKERS AND TAPS, NO FURTHER ANALYSIS
        # markers performance
        'num_markers_onsets': len(onsets_aligned['markers_onsets_input']),
        'num_markers_detected': onsets_aligned['verify_num_detected'],
        'num_markers_missed': onsets_aligned['verify_num_missed'],
        'markers_max_difference': onsets_aligned['verify_max_difference'],
        'markers_status': markers_status,
        'markers_ok': markers_ok,
        # Results computed in all onsets
        'num_stim_raw_all': onsets_aligned['num_stim_raw_onsets'],
        'num_resp_raw_all': onsets_aligned['num_resp_raw_onsets'],
        'ratio_resp_to_stim': 100.0 * float(np.size(onsets_aligned['resp_onsets_detected'])) / float(
            np.size(onsets_aligned['stim_onsets_input'], 0)),  # ratio of raw onsets

        # additional things: ITI, eventually also circular stats (phase of each tap wrt to circle of ~medITI)
        'iti': iti,
        'iti_med': iti_med,
        'iti_std': iti_std,
        'is_tapping_isochronous': STEADYTAPPING,
        'taps_rel_fullcycle': taps_rel,
        'taps_rel_whichfullcycle': which_cyc,
        'taps_rel_tapcycle': taps_rel_tapcycle,
        'taps_rel_whichtapcycle': which_cyc_tapcycle,
        'taps_rad_tapcycle': tap_rad,
        'taps_rad_stimcycle': tap_rad_cycledur,
        'circ_mean': circ_mean,
        'circ_std': circ_std,
        'async_ms': async_ms,
        'async_std_ms': async_std_ms,
        'cycledur': cycledur,
        'tapcycledur': tapcycledur,
        'cycle_points': cycle_points,
        'cyclestart_ms': cyclestart_ms,
        'cycle_ints': cycle_ints,
        'intervals': intervals,
        'async_naive': async_naive,
        'async_naive_frac': async_naive_frac,
    }
    is_failed = failing_criteria(analysis, config)
    return output, analysis, is_failed


def reformat_output(onsets_aligned):  # copied+modified from REPPAnalysis
    output = {
        # stim
        'stim_onsets_input': np.round(onsets_aligned['stim_onsets_input'], 2).tolist(),
        'stim_onsets_detected': np.round(onsets_aligned['stim_onsets_detected'], 2).tolist(),
        'stim_onsets_aligned': np.round(onsets_aligned['stim_onsets_aligned'], 2).tolist(),
        # response
        'resp_onsets_detected': np.round(onsets_aligned['resp_onsets_detected'], 2).tolist(),
        'resp_onsets_aligned': np.round(onsets_aligned['resp_onsets_aligned'], 2).tolist(),
        # markers
        'markers_onsets_input': np.round(onsets_aligned['markers_onsets_input'], 2).tolist(),
        'markers_onsets_detected': np.round(onsets_aligned['markers_onsets_detected'], 2).tolist(),
        'markers_onsets_aligned': np.round(onsets_aligned['markers_onsets_aligned'], 2).tolist(),
        'first_marker_detected': np.round(onsets_aligned['markers_onsets_detected'], 2)[0]
    }
    return output


def failing_criteria(analysis, config):
    """ Perform the failing criteria determined by the config parameters

    Parameters
    ----------

    analysis : dict
        A dictionary with the output of the performance analysis
    config : class
        Configuration parameters for the experiment (see ``config.py``)

    Returns
    --------

    is_failed : dict
        A dictionary with the output of the failing criteria
    """
    all_markers_are_detected = analysis['num_markers_onsets'] == analysis['num_markers_detected']
    markers_error_is_low = analysis['markers_max_difference'] < config.MARKERS_MAX_ERROR
    min_num_taps_is_ok = analysis['num_resp_raw_all'] >= config.MIN_RAW_TAPS
    max_num_taps_is_ok = analysis['num_resp_raw_all'] <= config.MAX_RAW_TAPS
    failed = not (
            all_markers_are_detected and markers_error_is_low and min_num_taps_is_ok and max_num_taps_is_ok)
    options = [all_markers_are_detected, markers_error_is_low, min_num_taps_is_ok, max_num_taps_is_ok]
    reasons = ["not all markers were detected", "markers error was too large", "too few detected taps",
               "too many detected taps"]
    if False in options:
        index = options.index(False)
        reason = reasons[index]
    else:
        reason = "All good"
    is_failed = {'failed': failed, 'reason': reason}
    return is_failed


def align_tapping_with_markers(
        markers_onsets,
        raw_extracted_onsets,
        markers_matching_window,
        onset_matching_window_phase,
        config,
        stim_info
):
    """
    Align tapping onsets with markers onsets only (beat detection task)
    """
    # get extracted onsets
    markers_detected_onsets = raw_extracted_onsets['markers_detected_onsets']
    tapping_detected_onsets = raw_extracted_onsets['tapping_detected_onsets']

    # use marker onsets to define a window for cleaning tapping onsets near markers
    min_marker_isi = np.min(np.diff(np.array(markers_onsets)))

    # tapping
    tapping_onsets_corrected = tapping_detected_onsets - markers_detected_onsets[0]
    delayms_tofirstdetectedmarker = markers_detected_onsets[0] - markers_onsets[0]

    # ideal  version of markers
    markers_onsets_aligned = markers_onsets - markers_onsets[0] + markers_detected_onsets[0]

    # find artifacts and remove
    markers_in_tapping = np.less(
        [(min(np.abs(onset - markers_onsets_aligned))) for onset in tapping_detected_onsets],
        min_marker_isi)  # find markers in the tapping signal: Note changed from

    # onset_matching_window to something based on marker ISI
    onsets_before_after_markers = np.logical_or(np.less(tapping_detected_onsets, min(markers_onsets_aligned)),
                                                np.less(max(markers_onsets_aligned),
                                                        tapping_detected_onsets))  # find onsets before/ after markers
    is_tapping_ok = np.logical_and(~markers_in_tapping, ~onsets_before_after_markers)  # are the tapping onsets ok?
    tapping_onsets_corrected = tapping_onsets_corrected[is_tapping_ok]  # filter markers from tapping
    tapping_detected_onsets = tapping_detected_onsets[
        is_tapping_ok]  # filter markers from tapping (using raw version)

    # verify markers
    onsets_detection_info = sp.verify_onsets_detection(
        markers_detected_onsets,
        markers_onsets,
        markers_matching_window,
        onset_matching_window_phase
    )

    # find good taps based on +/- 15% of median ITI
    if len(tapping_onsets_corrected) < 3:
        iti = []
        goodtaps = []
        async_naive = []
        async_naive_frac = []
        goodid = []
        async_ok = False
        iti_ok = False
        mediti = np.nan
        stditi = np.nan

    else:
        iti = np.diff(tapping_onsets_corrected)
        mediti = np.median(iti)
        tmp = np.nonzero(iti < 1.15 * mediti)
        tmp2 = np.nonzero(iti > 0.85 * mediti)
        goodid = np.intersect1d(tmp, tmp2)
        goodtapids = np.append(goodid[0], goodid + 1)
        goodtaps = tapping_detected_onsets[goodtapids]
        npercycle = config.NPERCYCLE
        firstid = config.ID_OF_CYCLE_START
        R = tapping_detected_onsets
        S = np.asarray(stim_info["stim_onsets"]) + config.STIM_BEGINNING  # np.asarray(stim_onsets_aligned)
        S = S + delayms_tofirstdetectedmarker
        taps_cycle = R - S[firstid]  # zero is onset of first cycle of rhythm
        onsets_cycle = S - S[firstid]  # stim also starts at 0
        cycle_points = onsets_cycle[firstid:firstid + npercycle + 1] - onsets_cycle[firstid]
        cycle_ints = np.diff(cycle_points)
        cycledur = np.sum(cycle_ints)
        if cycledur > 2000:  # assume it's a metronome (MAAAYYYBE need to make this more clever later....)
            cycledur = cycle_ints[0]
            onsets_cycle = S - S[0]  # stim also starts at 0
            cycle_points = onsets_cycle[0:npercycle + 1] - onsets_cycle[0]
            cycle_ints = cycle_points[0:2]
        stditi = np.std(iti[goodid])

        # calculate 1) whether median ITI is within 15% of cycledur or cycledur/2 or cycledur*2
        thresh = 0.15
        if (abs(mediti - cycledur) / cycledur) < thresh:
            iti_ok = True
        elif (abs(mediti * 2 - cycledur) / cycledur) < thresh:
            iti_ok = True
        elif (abs(mediti * 4 - cycledur) / cycledur) < thresh:
            iti_ok = True
        elif (abs(mediti / 2 - cycledur) / cycledur) < thresh:
            iti_ok = True
        else:
            iti_ok = False

        # and 2) whether mean (cycle) async < thresh*mediti
        taps_rel = np.mod(taps_cycle, cycledur)  # remainder is time in cycle
        asynchs = [(abs(taps_rel - i)) for i in np.cumsum(cycle_ints)]
        lengoodtaps = [len(a[a < (thresh * cycledur)]) for a in asynchs]
        allgoodasyncs = [a[a < (thresh * cycledur)] for a in asynchs]
        allasynch = np.array([])
        for a in allgoodasyncs:
            allasynch = np.concatenate([allasynch, a])
        async_ok = sum(lengoodtaps) > config.MIN_RAW_TAPS

        # for every tap, just find distance from nearest onset
        async_naive = np.array([])
        for r in R:
            tmp = r - S
            abstmp = abs(tmp)
            val, idx = min((val, idx) for (idx, val) in enumerate(abstmp))
            thisasync = tmp[idx]
            print(thisasync)
            async_naive = np.append(async_naive, thisasync)
        async_naive_frac = async_naive / cycledur

    aligned_onsets = {
        'resp_onsets_detected': tapping_detected_onsets,
        'resp_onsets_detected_good': goodtaps,
        'resp_onsets_corrected': tapping_onsets_corrected,
        'num_resp_raw_onsets': float(np.size(tapping_detected_onsets)),
        'markers_onsets_detected': markers_detected_onsets,
        'markers_onsets_aligned': markers_onsets_aligned,
        'markers_onsets_input': markers_onsets,
        'delayms_tofirstdetectedmarker': delayms_tofirstdetectedmarker,
        'asyncms_to_nearest_onset': async_naive,
        'asyncfrac_to_nearest_onset': async_naive_frac,
        **onsets_detection_info  # info about markers detection procedure
    }

    some_stats = {
        'iti': iti,
        'iti_median': mediti,
        'iti_std': stditi,
        'gooditi_ids': goodid,
        'async_ok': async_ok,
        'iti_ok': iti_ok,
        'mean_async': np.mean(async_naive)
    }

    return aligned_onsets, some_stats


###################################
# supporting functions for plotting
###################################


def plot_alignment(mxlim, aligned_onsets, position_subplot, is_zoomed, config):  # plot the original recording
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    Rraw = aligned_onsets['resp_onsets_detected']
    Sraw = aligned_onsets['stim_onsets_detected']
    stim_onsets_aligned = aligned_onsets['stim_onsets_aligned']
    resp_onsets_aligned = Rraw  # aligned_onsets['resp_onsets_aligned']
    num_aligned = np.sum(~np.isnan(resp_onsets_aligned))
    markers_onsets_detected = aligned_onsets['markers_onsets_detected']
    markers_onsets_aligned = aligned_onsets['markers_onsets_aligned']
    stim_raw = Sraw
    resp_raw = Rraw
    max_proximity_phase = config.ONSET_MATCHING_WINDOW_PHASE
    max_proximity = config.ONSET_MATCHING_WINDOW_MS
    if len(markers_onsets_aligned) > 0:
        plt.plot(np.array(markers_onsets_aligned) / 1000.0, 0.25 * np.ones(np.size(markers_onsets_aligned)), 'dm')
    if len(markers_onsets_detected) > 0:
        plt.plot(np.array(markers_onsets_detected) / 1000.0, 0.75 * np.ones(np.size(markers_onsets_detected)), 'bs')

    for j, _ in enumerate(stim_raw):
        # compute ISI before and after
        if j == 0:
            stim_next = stim_raw[j + 1] - stim_raw[j]
            stim_last = stim_next

        elif (j + 1) == len(stim_raw):
            stim_last = stim_raw[j] - stim_raw[j - 1]
            stim_next = stim_last
        else:
            stim_next = stim_raw[j + 1] - stim_raw[j]
            stim_last = stim_raw[j] - stim_raw[j - 1]

        for k, _ in enumerate(resp_raw):
            stim_proposal = stim_raw[j]
            resp_proposal = resp_raw[k]

            if resp_proposal > stim_proposal:
                phase = (resp_proposal - stim_proposal) / stim_next
            else:
                phase = (stim_proposal - resp_proposal) / stim_last
            distance_ms = abs(stim_proposal - resp_proposal)
            if (distance_ms < max_proximity) and (phase < max(max_proximity_phase)) and (
                    phase > min(max_proximity_phase)):
                plt.plot([resp_proposal / 1000.0, stim_proposal / 1000.0], [0, 1], 'y--')
            if distance_ms < max_proximity:
                plt.plot(resp_proposal / 1000.0, 0, 'cx')
            if (phase < max(max_proximity_phase)) and (phase > min(max_proximity_phase)):
                plt.plot(resp_proposal / 1000.0, 0, 'bx')
    #             if (phase<max(max_proximity_phase) and phase>min(max_proximity_phase)):
    #                 plt.plot([resp_proposal/1000.0,stim_proposal/1000.0],[0,1],'c--')
    if is_zoomed:
        min_x, max_x = find_min_max(Rraw, aligned_onsets, config)
        plt.xlim([min_x, max_x])
        plt.title("Zoomed view: Aligned onsets")
    else:
        plt.xlim(mxlim)
        plt.title("Aligned onsets Rraw={} Sraw={} aligned={}".format(len(Rraw), len(Sraw), num_aligned))
    if len(Rraw) > 0:
        plt.plot(np.array(Rraw) / 1000.0, 0 * np.ones(np.size(Rraw)), '+k')
    plt.plot(np.array(Sraw) / 1000.0, 1 * np.ones(np.size(Sraw)), 'og')
    for ll in range(len(stim_onsets_aligned)):
        plt.plot([resp_onsets_aligned[ll] / 1000.0, stim_onsets_aligned[ll] / 1000.0], [0, 1], 'k-')
        plt.plot(resp_onsets_aligned[ll] / 1000.0, 0, 'rx')


def plot_markers_zoomed_end(tt, audio_signals, aligned_onsets, position_subplot, config):
    # plot markers detection plot: zoomed
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    plt.plot(tt, audio_signals['rec_markers_final'], 'b-')
    plt.plot(tt, audio_signals['rec_test_final'], 'r-')
    message = 'Zoomed view on end markers: (test signal in red)'
    plt.title(message)
    y = np.max(audio_signals['rec_markers_final'])
    repp.analysis.plot_markers(y, aligned_onsets, config)
    plt.xlim([max(tt) - 4, max(tt) - 3])


def plot_tapping_rec(tt, mxlim, audio_signals, aligned_onsets, analysis, position_subplot, config):
    # plot tapping recording
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    rec_tap_final = audio_signals['rec_tapping_clean']
    Rraw = aligned_onsets['resp_onsets_detected']
    Sraw = aligned_onsets['stim_onsets_detected']
    plt.plot(tt, rec_tap_final, 'k-')
    message = 'Tapping detection: \n {:2.0f} of {:2.0f} stim onsets ({:2.2f}% ratio)'.format(
        analysis["num_resp_raw_all"],
        analysis["num_stim_raw_all"],
        analysis["ratio_resp_to_stim"])
    plt.title(message)
    if len(Rraw) > 0:
        plt.plot(Rraw / 1000.0, np.max(rec_tap_final) * config.EXTRACT_THRESH[1] * np.ones(np.size(Rraw)), 'xr')
    plt.plot(Sraw / 1000.0, np.max(rec_tap_final) * config.EXTRACT_THRESH[0] * np.ones(np.size(Sraw)), 'og')
    plt.plot(aligned_onsets['markers_onsets_aligned'] / 1000,
             config.EXTRACT_THRESH[0] * np.ones(np.size(aligned_onsets['markers_onsets_aligned'])), 'gs')
    plt.xlim(mxlim)


def plot_tapping_zoomed(tt, audio_signals, aligned_onsets, analysis, position_subplot, config):
    # plot tapping response: zoomed
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    rec_downsampled = audio_signals['rec_downsampled']
    rec_tap_final = audio_signals['rec_tapping_clean']
    Rraw = aligned_onsets['resp_onsets_detected']
    Sraw = aligned_onsets['stim_onsets_aligned']
    plt.plot(tt, rec_downsampled)
    plt.plot(tt, rec_tap_final)
    message = 'Zoomed view on tapping: \n {} raw resp onsets out of {} stim onsets ({:2.2f}%)'.format(
        analysis["num_resp_raw_all"],
        analysis["num_stim_raw_all"],
        analysis["ratio_resp_to_stim"])
    plt.title(message)
    if len(Rraw) > 0:
        plt.plot(Rraw / 1000.0, np.max(rec_tap_final) * config.EXTRACT_THRESH[1] * np.ones(np.size(Rraw)), 'xr')
    plt.plot(Sraw / 1000.0, np.max(rec_tap_final) * config.EXTRACT_THRESH[0] * np.ones(np.size(Sraw)), 'og')
    plt.plot(aligned_onsets['markers_onsets_aligned'] / 1000,
             config.EXTRACT_THRESH[0] * np.ones(np.size(aligned_onsets['markers_onsets_aligned'])), 'gs')
    min_x, max_x = find_min_max(Rraw, aligned_onsets, config)
    # set xlim to ~5 s in the middle of the stim
    xlen = max_x - min_x
    if xlen > 5:
        plt.xlim([np.floor(min_x + xlen / 2 - 2.5), np.ceil(min_x + xlen / 2 + 5)])  # [min_x, max_x])
    else:
        plt.xlim([min_x, max_x])


def plot_hist_iti(mxlim, aligned_onsets, analysis, position_subplot, config):
    # plot tapping inter-tap-intervals
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    iti = analysis['iti']
    tapcycledur = analysis['tapcycledur']
    iti_med = np.median(iti)
    iti_std = np.std(iti)
    plt.hist(iti, 100, density=True, facecolor='g', alpha=0.75)
    plt.xlabel('Inter-tap interval (s)')
    plt.ylabel('N')
    plt.title('medITI={:2.2f} ms, stdITI={:2.2f} ms, TAPCYCLE = {} ms'.format(
        iti_med,
        iti_std,
        round(tapcycledur))
    )


def plot_cycle_rasters(
        fig,
        analysis,
        position_subplot,
        config,
        stim_info,
        stim_prepared_filename
):  # plot delay interval and ISI
    # plot raster of taps by cycle on top of stimulus
    # THREE plotting options: WAVEFORM, ONSETS, SPECTROGRAM in config.PLOT_TYPE

    ax1 = fig.add_subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)

    cycledur = analysis['cycledur']
    cycle_points = analysis['cycle_points']
    taps_rel = analysis['taps_rel_fullcycle']
    which_cyc = analysis['taps_rel_whichfullcycle']
    intervals = analysis['intervals']

    # calculate whether each cycle_points is actually played
    onset_is_played = stim_info['onset_is_played']
    firstid = config.ID_OF_CYCLE_START  # npercycle * ncyc_baseline
    npercycle = config.N_ONSETS_PER_CYCLE
    cycle_onset_is_played = onset_is_played[firstid:firstid + npercycle + 1]

    if len(which_cyc) > 0:
        ncycles = max(which_cyc)
        ax1.plot(taps_rel, which_cyc, 'or')
        ax1.set_xlim([0, cycledur])
        ax1.set_ylabel('Cycle number', color='k')
        ax1.set_xlabel('Time (ms)')

        # ONSETS: plot raster on top of vertical lines representing onsets
        if config.PLOT_TYPE == 'ONSETS':
            for idx, p in enumerate(cycle_points[:-1]):
                if cycle_onset_is_played[idx] == 1:
                    ax1.plot([p, p], [0, ncycles], 'k-', linewidth=3)
                else:
                    ax1.plot([p, p], [0, ncycles], 'k-', linewidth=1)
            ax1.plot([cycle_points[-1], cycle_points[-1]], [0, ncycles], 'k--', linewidth=2)

        # WAVEFORM: dual y-axis plot, waveform goes between -1 and +1, representing 1/3 of vertical height of y-axis
        elif config.PLOT_TYPE == 'WAVEFORM':

            # get one cycle of audio
            spfs, stim_prepared = wav.read(stim_prepared_filename)
            cyclestart_ms = config.STIM_BEGINNING + config.CYCLE_START_S * 1000
            cyclestart_id = int(np.round(cyclestart_ms / 1000 * spfs))
            cycledur_samples = int(np.round(cycledur / 1000 * spfs))
            cycle_waveform = stim_prepared[cyclestart_id:cyclestart_id + cycledur_samples]

            # now plot waveform on second y-axis
            cycle_waveform = cycle_waveform / max(abs(cycle_waveform))  # scale to abs max 1
            ax2 = ax1.twinx()
            ax2.plot(np.array(range(0, len(cycle_waveform))) / spfs * 1000, cycle_waveform, 'b-')
            ax2.set_ylabel('Amplitude', color='b')

            # compute ylim so that waveform takes ~1/3 of plot and raster the other 2/3
            ax2.set_ylim(-1, 5)
            ax2.set_xlim([0, cycledur])

            # ax1.set_ylabel('Cycle number', color='k')
            # ax = plt.gca()
            ax1_yheight = ncycles * 12 / 7  # close to 2/3 but leave some space
            ax1.set_ylim(ncycles - ax1_yheight, ncycles)

        # SPECTROGRAM: dual y-axis plot, spectrogram represents 1/3 of vertical height of y-axis
        elif config.PLOT_TYPE == 'SPECTROGRAM':

            # get one cycle of audio
            spfs, stim_prepared = wav.read(stim_prepared_filename)
            cyclestart_ms = config.STIM_BEGINNING + config.CYCLE_START_S * 1000
            cyclestart_id = int(np.round(cyclestart_ms / 1000 * spfs))
            cycledur_samples = int(np.round(cycledur / 1000 * spfs))
            cycle_waveform = stim_prepared[cyclestart_id:cyclestart_id + cycledur_samples]

            # compute + plot spectrogram
            f, t, Sxx = signal.spectrogram(cycle_waveform, spfs)
            vmin = np.min(Sxx)
            vmax = np.max(Sxx) * config.SCALEFAC_SPECTROGRAM
            ax2 = ax1.twinx()
            ax2.pcolormesh(t * 1000, f, Sxx, shading='gouraud', vmin=vmin, vmax=vmax)
            ax2.set_ylabel('Frequency (Hz)', color='b')

            # compute ylim so that spectrogram takes ~1/3 of plot and raster the other 2/3
            ax2.set_ylim(-1, max(f) * 3)
            ax2.set_xlim([0, cycledur])

            # now the raster
            ax1_yheight = ncycles * 12 / 7  # close to 2/3 but leave some space
            ax1.set_ylim(ncycles - ax1_yheight, ncycles)

        else:
            'error! config.PLOT_TYPE should be set to ONSETS, WAVEFORM, or SPECTROGRAM'

        # set title
        if len(intervals) < 5:  # assume that intervals may not all be equal
            titlestr = "Cycle duration {} ms\nIntervals: | ".format(int(cycledur))
            for i in range(len(intervals)):
                titlestr += "{} | ".format(int(intervals[i]))
        else:  # assume a constant interval duration with pattern in cycle_onset_is_played
            titlestr = "Cycle duration {} ms\nPattern: ".format(int(cycledur))
            for i in range(len(cycle_onset_is_played)):
                titlestr += "{} ".format(int(cycle_onset_is_played[i]))
            plt.title(titlestr)

        # plot circular histogram
        plot_circ_hist(analysis, 11, config)


def plot_circ_hist(analysis, position_subplot, config):  # plot delay interval and ISI

    taps_rad_tapcycle = analysis['taps_rad_tapcycle']
    taps_rad_stimcycle = analysis['taps_rad_stimcycle']
    async_ms = analysis['async_ms']
    async_std_ms = analysis['async_std_ms']
    STEADYTAPPING = analysis['is_tapping_isochronous']
    iti_std = analysis['iti_std']
    iti_med = analysis['iti_med']

    theta = np.linspace(0.0, 2 * np.pi, 40, endpoint=False)
    if config.CYCLE_TYPE == 'OBJECTIVE':
        radii, _ = np.histogram(taps_rad_stimcycle, theta)
        add_string = config.CYCLE_TYPE
    elif config.CYCLE_TYPE == 'SUBJECTIVE':
        radii, _ = np.histogram(taps_rad_tapcycle, theta)
        add_string = config.CYCLE_TYPE
    else:
        radii = np.zeros(len(theta)-1)
        'error! config.CYCLE_TYPE should be set to OBJECTIVE or SUBJECTIVE'
        add_string = '***check CONFIG.CYCLE_TYPE!!'
    width = (2*np.pi) / 40
    center = (theta[:-1] + theta[1:]) / 2
    ax = plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot, polar=True)
    ax.bar(center, radii, width=width, bottom=0, align='center')

    if STEADYTAPPING:
        titlestr = "Mean async = {:2.2f} +/- {:2.2f} ms {}".format(async_ms, async_std_ms, add_string)
    else:
        titlestr = 'NON-ISO TAPPING, {:2.2f}% error (stdITI/medITI) {}'.format(100 * iti_std / iti_med, add_string)
    plt.title(titlestr)


def find_min_max(Rraw, aligned_onsets, config):
    min_x = config.STIM_BEGINNING / 1000
    max_x = min_x + 5.0
    if len(Rraw) > 2:
        min_x = min(Rraw / 1000) - 0.5
        max_x = max(Rraw / 1000) + 0.5
    if len(Rraw) > 4:
        min_x = (Rraw[2] / 1000) - 0.5
        max_x = (Rraw[4] / 1000) + 0.5
    max_x = min(max_x, max(aligned_onsets['markers_onsets_detected'] / 1000))
    max_x = min(max_x, min_x + 10)
    return min_x, max_x


# supporting functions
def plot_async_feedback(aligned_onsets, config):  # copied + modified from iterated_tapping.py):

    # make a cool figure to show accuracy feedback based on asynchrony

    circle1 = plt.Circle((0, 0), 3, color='r')
    circle2 = plt.Circle((0, 0), 2, color='w')
    circle3 = plt.Circle((0, 0), 1, color='r')  # , clip_on=False)

    fig, ax = plt.subplots()  # note we must use plt.subplots, not plt.subplot
    ax.add_patch(circle1)
    ax.add_patch(circle2)
    ax.add_patch(circle3)
    ax.set_xlim((-5, 5))
    ax.set_ylim((-5, 5))
    ax.set_aspect('equal', adjustable='datalim')

    # and set each tap's async as distance from origin with angle set at random
    async_frac = aligned_onsets['asyncfrac_to_nearest_onset'] * 2 / config.ASYNC_THRESH  # so distance of 1 = 15%
    angles = np.random.random(len(async_frac)) * 2 * np.pi
    x_s, y_s = [], []
    for idx, val in enumerate(angles):
        x_s.append(async_frac[idx] * np.cos(val))
        y_s.append(async_frac[idx] * np.sin(val))
    plt.scatter(x_s, y_s, c="k", s=8, zorder=10)

    return fig
