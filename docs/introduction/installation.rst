=======================
Installation
=======================

REPP needs Python 3.x (tested using Python 3.7 and 3.9). It has only been tested in macOS.


Virtual environment
----------------------

1. Set up virtual environment. For example:

::

    pip3 install virtualenv
    pip3 install virtualenvwrapper
    export WORKON_HOME=$HOME/.virtualenvs
    mkdir -p $WORKON_HOME
    export VIRTUALENVWRAPPER_PYTHON=$(which python3)
    source $(which virtualenvwrapper.sh)
    mkvirtualenv repp --python $(which python3)


This will automatically activate the virtual environment, so you should see it between brackets in your terminal.

Note that for this example, we call the virtual environment repp, but you can give it any other name.

The following commands are optional, but they will be useful to easily activate your virtual environment from the terminal:

::

    echo "export VIRTUALENVWRAPPER_PYTHON=$(which python3)" >> ~/.zshrc
    echo "source $(which virtualenvwrapper.sh)" >> ~/.zshrc


In the future, you can activate the virtual environment by typing in the terminal:

::

    workon repp


Install REPP
--------------

2.  Clone _REPP_ into the user’s home directory (or another directory of your choice):

::

    cd ~
    git clone git@gitlab.com:computational-audition-lab/repp.git
    cd repp


3. Install the requirements:

::

    pip3 install -r requirements.txt



4. Install REPP (the -e flag makes the REPP code editable):

::

    pip3 install -e .


5. Verify successful installation:

::

    repp --version


You are now done with the installation and you can begin using REPP.
