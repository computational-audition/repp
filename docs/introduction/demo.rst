
====
Demo
====

If you haven't installed `REPP` yet, follow the instructions in the 'Installation' section.


1. From the main code folder (e.g., cd repp), navigate to demos folder, for example:

::

    cd ~/my-projects/repp/demos

2. Make sure you're inside the right virtual environment, for example:

::

    workon repp


3. Then, open jupyter notebook by typing the following in the terminal (inside the activated working environment):

::

    jupyter notebook


You can choose between two demos:

::

    1. SMS tapping: isochronus tapping or beat synchronization to music

    2. Unconstrained tapping: spontaneous tempo or fastest tempo

If you are using BigSur (macOS) you may see a `PortAudioError` when running the recording phase of the demos.
Installing `portaudio` should fix the problem:

::

    brew install portaudio
