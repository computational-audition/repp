<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Onset alignment &mdash; repp 1.2.6 documentation</title>
      <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
      <link rel="stylesheet" href="../_static/css/theme.css" type="text/css" />
      <link rel="stylesheet" href="../_static/css/custom.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="../_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
        <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
        <script src="../_static/jquery.js"></script>
        <script src="../_static/underscore.js"></script>
        <script src="../_static/doctools.js"></script>
    <script src="../_static/js/theme.js"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Beat finding" href="../reppextension/beat_detection.html" />
    <link rel="prev" title="Onset extraction" href="onset_extraction.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >
            <a href="../index.html" class="icon icon-home"> repp
          </a>
              <div class="version">
                1.2.6
              </div>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="../search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <p class="caption" role="heading"><span class="caption-text">Introduction</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../introduction/about.html">About</a></li>
<li class="toctree-l1"><a class="reference internal" href="../introduction/installation.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="../introduction/demo.html">Demo</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">REPP documentation</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../repp_steps/basic_usage.html">Basic usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="../repp_steps/config.html">Config</a></li>
<li class="toctree-l1"><a class="reference internal" href="../repp_steps/stimulus.html">Stimulus preparation</a></li>
<li class="toctree-l1"><a class="reference internal" href="../repp_steps/recording.html">Recording</a></li>
<li class="toctree-l1"><a class="reference internal" href="../repp_steps/analysis.html">Analysis</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Signal processing</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="onset_extraction.html">Onset extraction</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Onset alignment</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">REPP extension</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../reppextension/beat_detection.html">Beat finding</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="../index.html">repp</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="../index.html" class="icon icon-home"></a> &raquo;</li>
      <li>Onset alignment</li>
      <li class="wy-breadcrumbs-aside">
            <a href="../_sources/signal_processing/onset_alignment.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <section id="onset-alignment">
<h1>Onset alignment<a class="headerlink" href="#onset-alignment" title="Permalink to this headline"></a></h1>
<p>We next align the list of shifted stimulus onsets (<code class="docutils literal notranslate"><span class="pre">shifted_stimulus_onsets</span></code>) with the list of extracted tapping onsets (<code class="docutils literal notranslate"><span class="pre">tapping_detected_onsets</span></code>).
In this step, we always use the first marker onset in <code class="docutils literal notranslate"><span class="pre">markers_detected_onsets</span></code> as the single frame of reference.
The other markers can be used to estimate the markers’ detection error (see <cite>Performance Analysis</cite>). The output of this process is the aligned list of
stimulus onsets (<code class="docutils literal notranslate"><span class="pre">stim_onsets_aligned</span></code>) and the aligned list of tapping onsets (<code class="docutils literal notranslate"><span class="pre">resp_onsets_aligned</span></code>), where there is one onset for each stimulus
onset (or a NA to indicate a missing response).</p>
<p>Note that the ‘’onset_matching_window’’ should be selected based on the requirements of each individual experiment and the desired ISI.
We point to the extensive body of research on this topic to inform such decisions (see Repp, 2005; Repp &amp; Su, 2013). For example, it is common to use
fixed matching windows of 200-250 ms, as asynchronies are typically smaller than 150 ms (Repp 2005). However, it is worth noting that this selection only
affects edge cases: for most participants and tapping trials, the asynchrony will be smaller and therefore unaffected by the size of onset_matching_window.
Another aspect considered in the onset alignment process is the fact that participants tend to tap with a negative and fixed mean asynchrony (Repp 2005).
Because of this, we define a matching window symmetrically around the perceptual centre (<code class="docutils literal notranslate"><span class="pre">mean_asynchrony</span></code>).</p>
<dl class="py function">
<dt class="sig sig-object py" id="repp.signal_processing.align_onsets">
<span class="sig-prename descclassname"><span class="pre">repp.signal_processing.</span></span><span class="sig-name descname"><span class="pre">align_onsets</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">initial_onsets</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">raw_extracted_onsets</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">markers_matching_window</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">onset_matching_window</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">onset_matching_window_phase</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="../_modules/repp/signal_processing.html#align_onsets"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#repp.signal_processing.align_onsets" title="Permalink to this definition"></a></dt>
<dd><p>Align tapping and stimulus onsets, as well as detected and known markers onsets</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>initial_onsets</strong> (<em>dict</em>) – list of initial onsets including stimulus and markers</p></li>
<li><p><strong>raw_extracted_onsets</strong> (<em>dict</em>) – list of extracted onsets including tapping and markers</p></li>
<li><p><strong>markers_matching_window</strong> (<em>float</em>) – Matching window to detect markers onsets corresponding to the known markers onsets.</p></li>
<li><p><strong>markers_matching_window</strong> – Matching window to detect tapping onsets corresponding to the stimulus onsets.</p></li>
<li><p><strong>onset_matching_window</strong> (<em>float</em>) – Matching window to detect tapping onsets corresponding to the stimulus onsets (ms).</p></li>
<li><p><strong>onset_matching_window_phase</strong> (<em>float</em>) – Matching window to detect tapping onsets corresponding to the stimulus onsets.</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p>Returns re-aligned onsets with key information (dict).</p>
</dd>
</dl>
</dd></dl>

<dl class="py function">
<dt class="sig sig-object py" id="repp.signal_processing.compute_matched_onsets">
<span class="sig-prename descclassname"><span class="pre">repp.signal_processing.</span></span><span class="sig-name descname"><span class="pre">compute_matched_onsets</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">stim_raw</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">resp_raw</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">max_proximity</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">max_proximity_phase</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="../_modules/repp/signal_processing.html#compute_matched_onsets"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#repp.signal_processing.compute_matched_onsets" title="Permalink to this definition"></a></dt>
<dd><p>Execute the matching onsets procedure</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>stim_raw</strong> (<em>numpy.ndarray</em>) – Stimulus onsets.</p></li>
<li><p><strong>resp_raw</strong> (<em>numpy.ndarray</em>) – Response onsets.</p></li>
<li><p><strong>max_proximity</strong> (<em>float</em>) – Matching window to detect onsets.</p></li>
<li><p><strong>max_proximity_phase</strong> (<em>float</em>) – Matching window to detect onsets in the matching phase.</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p>Returns the re-aligned onsets with key information (dict).</p>
</dd>
</dl>
</dd></dl>

<dl class="py function">
<dt class="sig sig-object py" id="repp.signal_processing.mean_asynchrony">
<span class="sig-prename descclassname"><span class="pre">repp.signal_processing.</span></span><span class="sig-name descname"><span class="pre">mean_asynchrony</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">stim_raw</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">resp_raw</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">max_proximity</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">max_proximity_phase</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="../_modules/repp/signal_processing.html#mean_asynchrony"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#repp.signal_processing.mean_asynchrony" title="Permalink to this definition"></a></dt>
<dd><p>Calculate mean of asynchrony</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>stim_raw</strong> (<em>numpy.ndarray</em>) – Stimulus onsets.</p></li>
<li><p><strong>resp_raw</strong> (<em>numpy.ndarray</em>) – Response onsets.</p></li>
<li><p><strong>max_proximity</strong> (<em>float</em>) – Matching window to detect onsets.</p></li>
<li><p><strong>max_proximity_phase</strong> (<em>float</em>) – Matching window to detect onsets in the matching phase.</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p>Returns the mean asynchrony</p>
</dd>
</dl>
</dd></dl>

<dl class="py function">
<dt class="sig sig-object py" id="repp.signal_processing.raw_onsets_to_matched_onsets">
<span class="sig-prename descclassname"><span class="pre">repp.signal_processing.</span></span><span class="sig-name descname"><span class="pre">raw_onsets_to_matched_onsets</span></span><span class="sig-paren">(</span><em class="sig-param"><span class="n"><span class="pre">stim_raw</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">resp_raw</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">max_proximity</span></span></em>, <em class="sig-param"><span class="n"><span class="pre">max_proximity_phase</span></span></em><span class="sig-paren">)</span><a class="reference internal" href="../_modules/repp/signal_processing.html#raw_onsets_to_matched_onsets"><span class="viewcode-link"><span class="pre">[source]</span></span></a><a class="headerlink" href="#repp.signal_processing.raw_onsets_to_matched_onsets" title="Permalink to this definition"></a></dt>
<dd><p>Match stim-response onsets from raw extracted onsets</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><ul class="simple">
<li><p><strong>stim_raw</strong> (<em>numpy.ndarray</em>) – Stimulus onsets.</p></li>
<li><p><strong>resp_raw</strong> (<em>numpy.ndarray</em>) – Response onsets.</p></li>
<li><p><strong>max_proximity</strong> (<em>float</em>) – Matching window to detect onsets.</p></li>
<li><p><strong>max_proximity_phase</strong> (<em>float</em>) – Matching window to detect onsets in the matching phase.</p></li>
</ul>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><p>Returns matched onsets, asynchrony, and ioi</p>
</dd>
</dl>
</dd></dl>

</section>


           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="onset_extraction.html" class="btn btn-neutral float-left" title="Onset extraction" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="../reppextension/beat_detection.html" class="btn btn-neutral float-right" title="Beat finding" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2021, Manuel Anglada-Tort, Peter Harrison, Nori Jacoby.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>