===========
Recording
===========

In the recording phase, the prepared stimulus (``stim_prepared``) is played through the laptop speakers and the resulting signal is simultaneously recorded
with the tapping response using the laptop’s single-channel microphone. The result is a wave file containing a single
channel recording with all recorded information: the markers, audio stimulus, tapping response, and any
additional noise from the background environment.

.. image:: ../_static/images/recording.jpeg
  :alt: recording