======================
Stimulus preparation
======================

Prior to conducting an experiment, the audio stimulus needs to be prepared to be used with the technology.
:class:`~repp.stimulus.REPPStimulus` provides the methods for this stimulus preparation step.

The stimulus can be be prepared by loading the audio and onset information from files:

::

    from repp.stimulus import REPPStimulus
    stimulus = REPPStimulus("stim_music1", config = non_adaptive_tapping) # specify stimulus name and the parameters configuration
    stim_prepared, stim_info, filenames = stimulus.prepare_stim_from_files(input_dir)  # prepare stimulus from file


It can also be prepared on the fly by either giving a list of inter-onset-intervals (ioi) or stimulus onsets:

::

    stim_ioi = np.repeat(500, 10) # a stimulus defined by a list of ioi
    stimulus = REPPStimulus("iso_500ioi", config = non_adaptive_tapping)
    stim_onsets = stimulus.make_onsets_from_ioi(stim_ioi)
    stim_prepared, stim_info, filenames = stimulus.prepare_stim_from_onsets(stim_onsets)



The stimulus preparation is organised in three steps:

1. Input

2. Spectral filtering

3. Add markers and shift onsets


1. Input
---------

In the simplest case, the input to the technology is a list of onsets (``stim_onsets``) or a list of IOIs (``stim_ioi``)
defining a given stimulus. An audio file ``stim`` is then generated from ``stim_onsets`` by placing a fixed sound
(e.g., a drum hit or short beep) for each onset in the list. We also need to input a list of booleans (True/ False)
to indicate whether each onset in stim_onsets is played or not (``onset_is_played``).  This is useful when working
with more complex stimuli, such as music or stimulus with “virtual onsets” where the onsets are not clearly defined in the audio signal.


Alternatively, the technology can input an audio file directly from a file, as long as ``stim_onsets`` and
``onset_is_played`` are also provided.


2. Spectral filtering
-----------------------

The next step consists of filtering the audio stimulus (``stim``) to ensure we can extract the tapping onsets from the recording
in the onset extraction step. Note that we use the same signal filtering procedure for all bandpass filters used in the technology.
Namely, we implement a spectral filtering using the scipy signal processing toolbox (Virtanen et al., 2020). We used a Butterworth filter with
automatic order selection (using buttord function with 3dB maximum loss in the passband and 9dB minimum attenuation in the stopband).
To avoid phase artifacts we used the filtfilt function to apply the filter forward and backward obtaining a zero phase shift.
We discovered empirically that better results are obtained when the filtfilt is applied twice. This results in applying Butterworth
filter 4 times on the signal. Consequently, we filter ``stim`` using this filtering procedure to remove those audio components that
are within a specified range (given in ``config.py``).


3. Add markers and shift onsets
--------------------------------

The next step consists of adding markers at the beginning and end of the filtered stimulus (``stim_filtered``).
These markers are critical to precisely align the tapping onsets with the stimulus onsets. Thus, to ensure the technology works,
these markers must be unambiguously identified in the onset alignment step, regardless of participants' hardware and software.
The markers must also be robust to adaptive noise-cancelling technologies, which can significantly alter the signal played by the computer.
To achieve this, the markers should be played at low frequencies and at nearly maximum volume.

To create the final stimulus, we shift ``stim_onsets`` to take into account the added markers (see Figure below),
returning a list of shifted stimulus onsets (``shifted_stim_onsets``). We repeat the same process to add the markers at the end of ``stim``,
resulting in ``prepared_stim````.

.. image:: ../_static/images/shifted_onsets.png
  :alt: shifted_onsets


.. automodule:: repp.stimulus
    :show-inheritance:
    :members:
