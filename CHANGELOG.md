# Change Log
All important changes to the repp package will be documented here.

The changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) and the project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.2.6]
### Added
- Beat finding code contributed by Vani Rajendran

## [v1.2.5]
### Added
- Made repo public
- Updated documentation to publication
- Fixed issue #10: stereo audio
- Added debug demo and folder

## [v1.2.4]
### Added
- Added unittest for basic analyses
- Benchmark for performance speed

## [v1.2.3]
### Added
- Fixed bug in alignment
- Updated Readme
- Code shared in beta phase

## [v1.2.2]
### Added
- Added relative phase + alignment plots
- Improved documentation
- Improved requirements
- Python 3.9

## [v1.2.0]
### Added
- Main classes with all methods: REPPStimulus anb REPPAnalysis
- New module organisation 
- Docs

## [v1.1.1]
### Added
- Minor refactoring of low-level functions to increase clarity
- First round of complete docs


## [v1.1.0]
### Added
- Added stimulus preparation step
- Added onset extraction step
- Added onset alignment step
- Added analysis step
- Added jupyter notebook demos
- Added basic docs
