# REPP: online SMS experiments

**Manuel Anglada-Tort, Peter Harrison, and Nori Jacoby**\
[Computational Auditory Perception Group](https://www.aesthetics.mpg.de/en/research/research-group-computational-auditory-perception.html), \
Max Planck Institute for Empirical Aesthetics.


_REPP (Rhythm ExPeriment Platfornm)_ is a python package for measuring
sensorimotor synchronization (SMS) in laboratory and online settings.

## Documentation

REPP documentation: https://computational-audition.gitlab.io/repp/

Source code: https://gitlab.com/computational-audition/repp


## Citation

Please cite this package if you use it:

Anglada-Tort, M., Harrison, P.M.C. & Jacoby, N. REPP: A robust cross-platform solution for online sensorimotor synchronization experiments. Behavior Reseasrch Methods (2022). https://doi.org/10.3758/s13428-021-01722-2


**Tapping datasets** supporting the paper (2022): https://osf.io/r2pxd/


## REPP Contributors

**Vani Rajendran**: developed analysis and plotting methods for beat detection tasks (see `beatfinding_cyclic` demo).


## Installation (macOS)
_REPP_ needs Python 3.x (tested using Python 3.7 and 3.9). It has only been tested in macOS.


### Virtual environment

1. Set up a virtual environment. For example:

```
pip3 install virtualenv

pip3 install virtualenvwrapper

export WORKON_HOME=$HOME/.virtualenvs

mkdir -p $WORKON_HOME

export VIRTUALENVWRAPPER_PYTHON=$(which python3)

source $(which virtualenvwrapper.sh)

mkvirtualenv repp --python $(which python3)
```

This will automatically activate the virtual environment, so you should see it between brackets in your terminal.

Note that for this example, we call the virtual environment __repp__, but you can give it any other name.
<br><br>
The following commands are optional, but they will be useful to easily activate your virtual environment from the terminal.

```
echo "export VIRTUALENVWRAPPER_PYTHON=$(which python3)" >> ~/.zshrc

echo "source $(which virtualenvwrapper.sh)" >> ~/.zshrc
```

In the future, you can activate this virtual environment by typing in the terminal:
```
workon repp
```


### Install REPP

2. Download or clone `repp` from the repository. For example:
```
git clone git@gitlab.com:computational-audition-lab/repp.git
```

3. Then make sure you are working from that folder in the terminal:
```
cd repp
```
4. Install the requirements:
```
pip3 install -r requirements.txt
```
5. Install REPP:
```
pip3 install -e .
```
The -e flag makes the REPP code editable.

### Verify successful installation:
```
repp --version
```
You are now done with the installation and you can begin using REPP.

## Requirements

The following python libraries need to be installed beside Python to run _REPP_ (see point 3, installing  `requirements.txt`) :
* scipy -- https://pypi.org/project/scipy, tested in version 1.5.3 (or older if using python 3.9)
* matplotlib -- https://pypi.org/project/matplotlib, tested in version 3.3.1
* click -- https://pypi.org/project/click, for command line comands, tested in version 7.1.2

To run the demo you will need:
* jupyter notebook -- https://jupyter.org
* sounddevice -- https://pypi.org/project/sounddevice, to record audio in the demos, tested in 0.4.1

If you are using BigSur (macOS) you may see a ``PortAudioError`` when running the recording phase of the demos. Installing ``portaudio`` 
(https://pypi.org/project/PyAudio/) should fix the problem:
```
brew install portaudio
```


## Contributing

We welcome contributions supporting new paradigms or improvements in the current code. Please talk to one of the authors if you would like to contribute.


## License

 MIT License

