import pytest
import os
import json
# repp
from repp.config import sms_tapping
from repp.analysis import REPPAnalysis
from reppextension.beat_detection import REPPAnalysisBeatDetection

analyzer = REPPAnalysis(config=sms_tapping)
analyzer_beat_detection = REPPAnalysisBeatDetection(config=sms_tapping)

here = os.path.abspath(os.path.dirname(__file__))

# read stim information
with open(os.path.join("output", "iso_500ioi.json"), "r") as f:
    stim_info_iso1 = json.loads(f.read())

with open(os.path.join("output", "stim_music1.json"), "r") as f:
    stim_info_music1 = json.loads(f.read())


# tests
def example_iso_with_plot():
    output, analysis, is_failed = analyzer.do_analysis(
        stim_info_iso1,
        os.path.dirname(here) + "/repp/tests/example_audio/example_iso1.wav",
        "example_output_iso1",
        os.path.dirname(here) + "/repp/tests/output/output_example_iso1.png"
    )

    assert len(output) == 12
    assert len(analysis) == 26
    assert len(is_failed) == 2


def example_iso_without_plot():
    output, analysis, is_failed = analyzer.do_only_stats(
        stim_info_iso1,
        os.path.dirname(here) + "/repp/tests/example_audio/example_iso1.wav"
    )

    assert len(output) == 12
    assert len(analysis) == 26
    assert len(is_failed) == 2


def example_music_with_plot():
    output, analysis, is_failed = analyzer.do_analysis(
        stim_info_music1,
        os.path.dirname(here) + "/repp/tests/example_audio/example_music1.wav",
        "example_output_music1",
        os.path.dirname(here) + "/repp/tests/output/output_example_music1.png"
    )

    assert len(output) == 12
    assert len(analysis) == 26
    assert len(is_failed) == 2


def example_music_without_plot():
    output, analysis, is_failed = analyzer.do_only_stats(
        stim_info_iso1,
        os.path.dirname(here) + "/repp/tests/example_audio/example_music1.wav"
    )

    assert len(output) == 12
    assert len(analysis) == 26
    assert len(is_failed) == 2


def example_beat_detection():
    jsonfile = open(os.path.dirname(here) + "/repp/output/stim_rhythm_P2.json")
    output, analysis, is_failed = analyzer_beat_detection.do_beat_detection_analysis(
        os.path.dirname(here) + "/repp/output/stim_rhythm_P2.wav",
        os.path.dirname(here) + "/repp/input/stim_rhythm_P2.wav",
        json.load(jsonfile),
        'DEMO stim_rhythm_P2',
        os.path.dirname(here) + "/repp/output/plot_stim_rhythm_P2.png",
)

    assert len(output) == 9
    assert len(analysis) == 31
    assert len(is_failed) == 2

# benchmarking
# to run 10 rounds and save as histogram:
# python3 -m pytest tests.py --benchmark-min-rounds=10 --benchmark-histogram 
# to run 10 rounds and save all data (not only the stats):
# python3 -m pytest tests.py --benchmark-min-rounds=10 --benchmark-json="output_benchmark.json"

def test_iso_with_plot(benchmark):
    benchmark(example_iso_with_plot)


def test_music_with_plot(benchmark):
    benchmark(example_music_with_plot)


def test_iso_without_plot(benchmark):
    benchmark(example_iso_without_plot)


def test_music_without_plot(benchmark):
    benchmark(example_music_without_plot)


def test_beat_detection(benchmark):
    benchmark(example_beat_detection)
