####################################################################################################
# File:     setup.py
# Purpose:  Setup of the package.
#
# Author:   Manuel Anglada-Tort, Peter Harrison, Nori Jacoby
####################################################################################################
import os

from setuptools import find_packages

with open(os.path.join("repp", "VERSION")) as version_file:
    version = version_file.read().strip()

from setuptools import setup, Command


class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system('rm -vrf ./build ./dist ./*.pyc ./*.tgz ./*.egg-info')


__MODULES__ = [
    "repp.cli",
    "repp.stimulus",
    "repp.analysis",
    "repp.signal_processing",
    "repp.utils",
    "repp.config",
    "reppextension.beat_detection"
]

setup(
    name="repp",
    version=version,
    py_modules=__MODULES__,
    author="Manuel Anglada-Tort, Peter Harrison, Nori Jacoby",
    author_email="manel.anglada.tort@gmail.com",
    description="Online technology for measuring sensorimotor synchronization",
    long_description="Online technology for measuring sensorimotor synchronization",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/computational-audition-lab/repp",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    include_package_data=True,
    package_data={"repp": ["VERSION"]},
    cmdclass={'clean': CleanCommand},
    install_requires=[
        "matplotlib",
        "sounddevice",
        "numpy",
        "scipy",
        "jupyter",
        "click"
    ],
    entry_points={
        "console_scripts": [
            "repp = repp.cli:repp"
        ]
    }
)

# python3.7 setup.py sdist bdist_wheel
